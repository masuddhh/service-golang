# Base image
FROM golang:latest

# Set working directory
WORKDIR /app

# Copy source code to working directory
COPY . .

# Build the binary
RUN go build -o main .

# Expose port 8080
EXPOSE 8080

# Start the application
CMD ["./main"]
